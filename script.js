/**
     * PopUp universal API для форм/блоков drupal
     * В инициализации прописать свои данные и все заработает для вашего селектора формы
     * Варианты появление пупап:
     * popupObj.one - Только после второй загрузки любой страницы через несколько секунд
     *
     * Если кто-то сможет перенести этот код по-нормальному в файл js, будет молодцом
     */
    jQuery(document).ready(function () {

        //Инициализация
        jQuery('<div id="popup_window_dark"></div>').insertAfter('.page-row#footer');
        var popupObj = {
                one: {
                    selector: '#block-webform-client-block-854', // К кому подключать, можно перечислять через запятую, стандарт jquery
                    minutes:  120, // Сколько минут жить такой логике
                    popup_show_post_time: 8000, // Задержка перед появлением попап в ms
                    popup_mini_show_post_time: 2000 // Задержка перед появлением попап-mini в ms
                },
                two: {
                    selector: '#block-webform-client-block-857', // К кому подключать, можно перечислять через запятую, стандарт jquery
                    minutes:  120, // Сколько минут жить такой логике
                    popup_show_post_time: 8000, // Задержка перед появлением попап в ms
                    popup_mini_show_post_time: 2000 // Задержка перед появлением попап-mini в ms
                }
                /*get name() {
                    return "Revilon";
                },
                set name(val) {
                    console.log(val);
                }*/
            },
            temp_selector = jQuery(popupObj.one.selector + ', ' + popupObj.two.selector), // При создании очередной логики добавить сюда селектор из объекта popupObj
            popup_cookie,
            date = new Date(),
            label_name,
            label_email,
            label_query,
            close_selectors = jQuery('.popup_window-close, #popup_window_dark'),
            dark_background = jQuery('#popup_window_dark'),
            domElement;
        temp_selector.each(function () {
            domElement = jQuery(this).find('.popup_mini').detach();
            jQuery(this).append(domElement);
        });
        close_selectors.addClass('active_element');

        //Проработка placeholder'ов
        label_name = temp_selector.find('#webform-component-name label').text();
        label_email = temp_selector.find('#webform-component-mail label').text();
        label_query = temp_selector.find('#webform-component-message label').text();
        temp_selector.find('#webform-component-name input').attr("placeholder", label_name).val("").focus().blur();
        temp_selector.find('#webform-component-mail input').attr("placeholder", label_email).val("").focus().blur();
        temp_selector.find('#webform-component-message textarea').attr("placeholder", label_query).val("").focus().blur();

        // Тут я напишу логику появления пупапа-формы popupObj.one и popupObj.two
        var SelectorForOne = jQuery(popupObj.one.selector),
            SelectorForTwo = jQuery(popupObj.two.selector);
        date.setTime(date.getTime() + (popupObj.one.minutes * 60 * 1000));
        popup_cookie = Number(jQuery.cookie('popup_window-1'));
        if(popup_cookie == 0) {
            jQuery.cookie('popup_window-1', '1');
            setTimeout(function() {
                var topCurrent = window.pageYOffset + 100;
                SelectorForTwo.show('fast').addClass('active_element').offset({top:topCurrent});
                dark_background.css('zIndex', '500').fadeTo(400, 0.7);
            }, popupObj.two.popup_show_post_time);
        } else if(popup_cookie === 1) {
            jQuery.cookie("popup_window-1", '2', {expires: date});
            setTimeout(function() {
                var topCurrent = window.pageYOffset + 100;
                SelectorForOne.show('fast').addClass('active_element').offset({top:topCurrent});
                dark_background.css('zIndex', '500').fadeTo(400, 0.7);
            }, popupObj.one.popup_show_post_time);
        } else if(popup_cookie >= 2) {
            SelectorForOne.show('fast');
            SelectorForOne.find('.content').first().hide();
            SelectorForTwo.show('fast');
            SelectorForTwo.find('.content').first().hide();
            setTimeout(function() {
                SelectorForOne.find('.popup_mini').css("margin-left","-113px").animate({"margin-left": '+=113'}).addClass('active_element');
            }, popupObj.one.popup_mini_show_post_time);
            setTimeout(function() {
                SelectorForTwo.find('.popup_mini').css("margin-left","-113px").animate({"margin-left": '+=113'}).addClass('active_element');
            }, popupObj.two.popup_mini_show_post_time);
        }

        //События на клики по форме
        close_selectors.click(function () {
            if(jQuery(this).hasClass("active_element")) {
                close_selectors.removeClass('active_element');
                dark_background.fadeTo(400, 0, function () {
                    jQuery(this).css('zIndex', '-110');
                });
                temp_selector.each(function () {
                    if(jQuery(this).hasClass("active_element")) {
                        jQuery(this).removeClass('active_element');
                        jQuery(this).find('.content').first().fadeTo(400, 0, function () {
                            jQuery(this).hide();
                        });
                        jQuery(this).find('.popup_mini').addClass('active_element');
                    }
                    jQuery(this).find('.popup_mini.active_element').css("margin-left","-113px").animate({"margin-left": '+=113'});
                });
            }
        });
        temp_selector.find('.popup_mini').click(function () {
            var topCurrent = window.pageYOffset + 100;
            if(jQuery(this).hasClass("active_element")) {
                jQuery(this).removeClass('active_element');
                close_selectors.addClass('active_element');
                jQuery(this).closest('.block-webform').addClass('active_element');
                jQuery(this).closest('.block-webform').find('.content').first().show();
                dark_background.css('zIndex', '500').fadeTo(400, 0.7);

                jQuery(this).closest('.block-webform').find('.content').first().fadeTo(400, 1).offset({top:topCurrent});
            }
            temp_selector.each(function () {
                jQuery(this).find('.popup_mini').css("margin-left","0px").animate({"margin-left": '-=113'});
            });
        });
    });
